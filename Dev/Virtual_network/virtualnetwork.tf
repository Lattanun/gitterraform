###########################
#  DEV VNET #
###########################
resource "azurerm_virtual_network" "vnet" {
  name                = local.virtual_network_name
  resource_group_name = local.resource_group_name
  location            = local.location
  address_space       = [local.virtual_network_address_prefix]
  tags = {
    Environment = "Development"
  }
}

resource "azurerm_subnet" "app_subnet" {
  name                 = local.subnet_application_name 
  resource_group_name  = local.resource_group_name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [local.subnet_application_address_prefix]
}
