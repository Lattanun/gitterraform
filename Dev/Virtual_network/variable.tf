locals {
  resource_group_name            = "rgterraformtest"
  virtual_network_name           = "vnettfdev001"
  virtual_network_address_prefix = "10.0.1.0/24"
  subnet_application_name        = "snettfdev001"
  subnet_application_address_prefix  = "10.0.1.0/24"
  location                       = "East Asia"

  organization = "terraform_lattanun_labtest"
}
