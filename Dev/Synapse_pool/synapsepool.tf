resource "azurerm_synapse_sql_pool" "main" {
  name                 = "examplesqlpool"
  synapse_workspace_id = local.synapse_workspace_id
  sku_name             = "DW100c"
  create_mode          = "Default"
}