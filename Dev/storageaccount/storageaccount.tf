resource "azurerm_storage_account" "main" {
  name                     = local.name
  resource_group_name      = local.resource_group_name
  location                 = local.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "staging"
  }
}

resource "azurerm_storage_data_lake_gen2_filesystem" "main" {
  name               = "adlstestdevtf001"
  storage_account_id = azurerm_storage_account.main
}