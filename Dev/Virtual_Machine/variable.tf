locals {
  nic_name            = "vmtfdev001-nic"
  location            = "East Asia"
  resource_group_name = "rgterraformtest"

  ip_configuration_name = "testconfiguration1"
  subnet_id             = "/subscriptions/50f1ce21-b040-4300-8490-a54c52c51723/resourceGroups/rgterraformtest/providers/Microsoft.Network/virtualNetworks/vnettfdev001/subnets/snettfdev001"
  vm_name             = "vmtfdev001"
}
