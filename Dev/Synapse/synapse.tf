


resource "azurerm_storage_data_lake_gen2_filesystem" "main" {
  name               = "adlstestdevtf001"
  storage_account_id = local.storage_account_id
}

resource "azurerm_synapse_workspace" "main" {
  name                                 = local.name
  resource_group_name                  = local.rgname
  location                             = local.location
  storage_data_lake_gen2_filesystem_id = azurerm_storage_data_lake_gen2_filesystem.main.id
  sql_administrator_login              = local.user
  sql_administrator_login_password     = local.password


  identity {
    type = "SystemAssigned"
  }

  tags = {
    Env = "TF_DEV"
  }

}