resource "azurerm_cognitive_account" "main" {

  name                = local.cognitive_account_name
  resource_group_name = local.resource_group_name
  location            = local.location
  kind                = "OpenAI"
  sku_name            = "S0"
}

resource "azurerm_cognitive_deployment" "main" {
  name                 = local.openai_name
  cognitive_account_id = azurerm_cognitive_account.main.id
  model {
    format  = "OpenAI"
    name    = local.openai_model_name
    version = local.version
  }
}


