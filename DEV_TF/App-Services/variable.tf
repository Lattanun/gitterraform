locals{
    resource_group_name         = data.terraform_remote_state.resourcegroup.outputs.resource_group_name
    location                    = "East Asia"
    app_service_name            = "demomfecappservice001"
    app_service_plan_id         = data.terraform_remote_state.app_service_plan.outputs.app_service_plan_id
    subnet_application_id       = data.terraform_remote_state.network.outputs.subnet_app_id

    organization                                = "terraform_lattanun_labtest"
}
