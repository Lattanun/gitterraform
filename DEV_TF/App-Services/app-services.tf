resource "azurerm_windows_web_app" "app_service" {
  name                = local.app_service_name
  location            = local.location
  resource_group_name = local.resource_group_name
  service_plan_id = local.app_service_plan_id
  https_only                = true
  virtual_network_subnet_id = local.subnet_application_id

  site_config {
    http2_enabled           =  true
    always_on               = true
    use_32_bit_worker       = false
    application_stack {
      current_stack  = "dotnet"
      dotnet_version = "v6.0"
    }
  }

  logs {
    http_logs{
      file_system{
        retention_in_days = 3
        retention_in_mb = 100
      }
    }
  }
}


#################################################################################
#####     Custom Domain #####

# resource "azurerm_app_service_custom_hostname_binding" "hostname" {
#   hostname            = local.hostname
#   app_service_name    = azurerm_app_service.app_service.name
#   resource_group_name = local.resource_group_name
# }

