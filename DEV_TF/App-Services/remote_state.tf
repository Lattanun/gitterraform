data "terraform_remote_state" "resourcegroup" {
  backend  = "remote" 
  config ={
    hostname = "app.terraform.io"
    organization = local.organization

    workspaces = {
      name = "Resource-Group-DEV"
    }
  }
}

data "terraform_remote_state" "network" {
  backend  = "remote" 
  config ={
    hostname = "app.terraform.io"
    organization = local.organization

    workspaces = {
      name = "Network-DEV"
    }
  }
}

data "terraform_remote_state" "app_service_plan" {
  backend  = "remote" 
  config ={
    hostname = "app.terraform.io"
    organization = local.organization

    workspaces = {
      name = "App-Service-Plan-DEV"
    }
  }
}
