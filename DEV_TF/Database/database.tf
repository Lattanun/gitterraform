resource "azurerm_mssql_server" "database_server" {
  name                         = local.database_server_name
  resource_group_name          = local.resource_group_name
  location                     = local.location
  version                      = "12.0"
  administrator_login          = local.admin_username
  administrator_login_password = local.admin_password
  minimum_tls_version          = "1.2"

  tags = {
    environment = "Development"
  }
}

resource "azurerm_mssql_virtual_network_rule" "vnetrule" {
  name      = "allow-vnet-app"
  server_id = azurerm_mssql_server.database_server.id
  subnet_id = local.subnet_application_id
}

resource "azurerm_mssql_database" "database" {
  name                          = local.database_name
  server_id                     = azurerm_mssql_server.database_server.id
  collation                     = local.database_collation
  create_mode                   = "Default"
  sku_name                      = local.database_sku
  auto_pause_delay_in_minutes   = "-1"
  storage_account_type          = "Zone" 

      long_term_retention_policy {
           monthly_retention = "PT0S"
           weekly_retention  = "P4W"
           yearly_retention  = "PT0S"
           week_of_year = "1"
        }
      short_term_retention_policy {
           retention_days = "7"
        }

  tags = {
    environment = "Development"
  }
}