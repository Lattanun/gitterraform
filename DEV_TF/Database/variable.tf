locals{
    location            = "East Asia"
    resource_group_name         = data.terraform_remote_state.resourcegroup.outputs.resource_group_name
    database_server_name        = "mfecdemodbserver001tf"
    database_private_ip         = "10.43.33.4"
    database_name               = "db001"
    database_collation          = "Thai_CI_AS"
    database_sku                = "Basic"
    admin_username              = "adminsa"
    admin_password              = "p2U7$d3wjVBvCm@$"
    subnet_application_id       = data.terraform_remote_state.network.outputs.subnet_app_id       
    subnet_database_id          = data.terraform_remote_state.network.outputs.subnet_db_id
    virtual_network_id          = data.terraform_remote_state.network.outputs.virtual_network_id

    organization                                = "terraform_lattanun_labtest"
}