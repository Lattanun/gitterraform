##### DNS Zone #####
resource "azurerm_private_dns_zone" "dnsprivatezone" {
  name                = "privatelink.database.windows.net"
  resource_group_name = local.resource_group_name
}

resource "azurerm_private_dns_a_record" "dnsrecord" {
  name                = local.database_server_name
  zone_name           = azurerm_private_dns_zone.dnsprivatezone.name
  resource_group_name = local.resource_group_name
  ttl                 = 300
  records             = [local.database_private_ip]
}

resource "azurerm_private_dns_zone_virtual_network_link" "dnszonelink" {
  name = "dnszonelink"
  resource_group_name = local.resource_group_name
  private_dns_zone_name = azurerm_private_dns_zone.dnsprivatezone.name
  virtual_network_id = local.virtual_network_id
}

##### Private Endpoint #####
resource "azurerm_private_endpoint" "privateendpoint" {
  name                = "database-privateendpoint"
  location            = local.location
  resource_group_name = local.resource_group_name
  subnet_id           = local.subnet_database_id
  ip_configuration {
    name = "database-private-ip"
    subresource_name                 = "sqlServer"
    private_ip_address = local.database_private_ip
  }

  private_dns_zone_group {
    name = "privatednszonegroup"
    private_dns_zone_ids = [azurerm_private_dns_zone.dnsprivatezone.id]
  }
  private_service_connection {
    name                              = "database-privateendpoint"
    private_connection_resource_id = azurerm_mssql_server.database_server.id
    is_manual_connection              = false
    subresource_names                 = ["sqlServer"]
  }
depends_on = [azurerm_mssql_server.database_server,azurerm_private_dns_zone.dnsprivatezone,azurerm_private_dns_zone_virtual_network_link.dnszonelink]
}

