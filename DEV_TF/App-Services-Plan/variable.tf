locals{
    location            = "East Asia"
    resource_group_name = data.terraform_remote_state.resourcegroup.outputs.resource_group_name
    app_service_plan_name = "appserviceplan001"
    app_service_plan_kind = "Windows"
    # app_service_plan_tier = "PremiumV3"
    # app_service_plan_size = "P1V3"
    app_service_plan_tier = "Basic"
    app_service_plan_size = "B1"
    
    organization                                = "terraform_lattanun_labtest"
}
