resource "azurerm_app_service_plan" "app_service_plan" {
  name                = local.app_service_plan_name
  location            = local.location
  resource_group_name = local.resource_group_name
  kind                = local.app_service_plan_kind

  sku {
    tier = local.app_service_plan_tier
    size = local.app_service_plan_size
  }

}
