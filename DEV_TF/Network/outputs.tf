# ################################
# #           Output             #   VNET_ID
# ################################
output "virtual_network_id" {
  value = azurerm_virtual_network.vnet.id
}



# ################################
# #           Output             #   SUBNET_ID
# ################################
# Application Subnet
output "subnet_app_id" {
  value = azurerm_subnet.app_subnet.id
}

output "subnet_db_id" {
  value = azurerm_subnet.db_subnet.id
}



