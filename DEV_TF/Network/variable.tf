locals{
    resource_group_name                         = data.terraform_remote_state.resourcegroup.outputs.resource_group_name
    virtual_network_name                        = "VNETtfmfecdev001"
    virtual_network_address_prefix              = "10.43.32.0/23"
    subnet_application_name                     = "SNETDEV-App"
    subnet_application_address_prefix           = "10.43.32.0/24"
    subnet_database_name                        = "SNETDEV-DB"
    subnet_database_address_prefix              = "10.43.33.0/24"
    location                                    = "East Asia"

    organization                                = "terraform_lattanun_labtest"
}



