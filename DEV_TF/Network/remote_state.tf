data "terraform_remote_state" "resourcegroup" {
  backend  = "remote" 
  config ={
    hostname = "app.terraform.io"
    organization = local.organization

    workspaces = {
      name = "Resource-Group-DEV"
    }
  }
}