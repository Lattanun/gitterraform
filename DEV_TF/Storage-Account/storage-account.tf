resource "azurerm_storage_account" "main" {

  name                = local.storage_account_name
  resource_group_name = local.resource_group_name
  location            = local.location

  account_tier              = "Standard"
  account_kind              = "StorageV2"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
  min_tls_version           = "TLS1_2"
  public_network_access_enabled = true

  network_rules {
    default_action             = "Deny"
    virtual_network_subnet_ids = [local.subnet_application_id]
  }
}


